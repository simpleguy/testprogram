﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace TestProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Overseer.check(args);
            Numeric a = new Numeric(args[0]);
            
            System.Console.WriteLine("В ДЕСЯТИЧНОЙ СИСТЕМЕ: " + a);
            a.convert8();
            System.Console.WriteLine("В ВОСЬМИРИЧНОЙ СИСТЕМЕ: " + a);
        }
    }
}
