﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram
{
    class Overseer
    {
        public static void check(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Должен быть один аргумет!");
                System.Environment.Exit(0);
            }
            //проверка на число
            for (int i = 0; i < args[0].Length; i++)
            {
                if (!Char.IsDigit(args[0][i]))
                {
                    Console.WriteLine("Аргумент должен быть числом!");
                    System.Environment.Exit(0);
                }
            }
        }
    }
}
