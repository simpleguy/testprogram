﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram
{
    class RussianParser : IParser
    {
        IList<List<string>> numList;
        IList<string> thousand;
        public RussianParser()
        {
            Init();
        }
        public void Init()
        {
            numList = new List<List<string>> // список из списков слов. еденицы десятки сотни и исключения вроде 11, 12  тд.
            {
                new List<string> { "", "один ", "два ", "три ", "четыре ", "пять ", "шесть ", "семь ", "восемь ", "девять " },
                new List<string> { "", "", "двадцать ", "тридцать ", "сорок ", "пятьдесят ", "шестьдесят ", "семьдесят ", "восемьдесят ", "девяносто " },
                new List<string> { "", "сто ", "двести ", "триста ", "четыреста ", "пятьсот ", "шестьсот ", "семьсот ", "восемьсот ", "девятьсот "},
                new List<string> { "десять ", "одиннадцать ", "двенадцать ", "тринадцать ", "четырнадцать ", "пятнадцать ", "шестнадцать ", "семнадцать ", "восемнадцать ", "девятнадцать " }
            };
            thousand = new List<string> { "",
                                        "тысяч",
                                        "миллион",
                                        "миллиард",
                                        "биллион",
                                        "биллиард",
                                        "триллион",
                                        "триллиард",
                                        "квадриллион",
                                        "квадриллиард",
                                        "квинтиллион",
                                        "квинтиллиард",
                                        "секстиллион",
                                        "секстиллиард",
                                        "септиллион",
                                        "септиллиард",
                                        "октиллион",
                                        "октиллиард",
                                        "нониллион",
                                        "нониллиард",
                                        "дециллион",
                                        "дециллиард",
                                        "ундециллион",
                                        "ундециллиард",
                                        "додециллион",
                                        "додециллиард",
                                        "тредециллион",
                                        "тредециллиард",
                                        "кваттуордециллион",
                                        "кваттуордециллиард",
                                        "квиндециллион",
                                        "квиндециллиард",
                                        "седециллион",
                                        "седециллиард" }; //тысячи милионы и т.д.
        }
        private String ParseTriplet(String str)
        {
            String res = "";
            int index, num, len = str.Length;
            for (int pos = len - 1; pos >= 0; pos--)
            {
                index = len - pos - 1;
                num = str[index] - 48;
                if (pos == 1 && num == 1) // исключения, уникальный случай 11, 12 и т.д.
                {
                    num = str[len - 1] - 48;
                    res += numList[3][num];
                    break; //больше чисел не будет в данном триплете
                }
                else
                {
                    res += numList[pos][num];
                }
            }   
            return res;
        }
        public String Parse(String val)
        {
            StringBuilder res = new StringBuilder();
            String resstr, number = "", tempstr = "";
            int tripletLen = 3, countTriplets = (val.Length + 2) / 3; // количество триплетов (с округлением в ольшую сторону)

            switch (val.Length % 3) // для кратности 3 заполняем начало нулями
            {
                case 1:
                    number += "00";
                    break;
                case 2:
                    number += "0";
                    break;
            }
            number += val;

            for (int i = 0, offset = 0; i < countTriplets; i++, offset += tripletLen) // так как первый триплет взяли считаем что нулевая итерация выполнена
            {
                tempstr = number.Substring(offset, tripletLen);
                res.Append(ParseTriplet(tempstr)); // отпарсить и добавить в результат
                res.Append(thousand[countTriplets - 1 - i]);
                if (countTriplets - 1 - i > 1) // если числа идут больше тысяч
                {
                    if (tempstr[tripletLen - 1] >= '2' && tempstr[tripletLen - 1] <= '4' && tempstr[tripletLen - 2] != '1') //выходи за пределы 
                    {
                        res.Append("а");
                    }
                    else if (tempstr[tripletLen - 1] == '0' || tempstr[tripletLen - 1] >= '5' || tempstr[tripletLen - 2] == '1')
                    {
                        res.Append("ов");
                    }
                }
                res.Append(" ");
            }
            //финальный штрих
            res.Replace("один тысяч", "одна тысяча");
            res.Replace("два тысяч", "две тысячи");
            res.Replace("три тысяч", "три тысячи");
            res.Replace("четыре тысяч", "четыре тысячи");
            //нужно проверить на ноль
            resstr = res.ToString();
            if (!isZero(resstr))
                return resstr;
            else
                return "ноль";
        }
        private bool isZero(String str)
        {
            for(int i = 0; i < str.Length; i++)
                if (Char.IsLetter(str[i]))
                    return false;
            return true;
        }
    }
}
