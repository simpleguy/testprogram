﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace TestProgram
{
    class Numeric
    {
        BigInteger number;
        IParser parser;

        public Numeric(String num = "0")
        {
            number = BigInteger.Parse(num);
            parser = new RussianParser();
        }

        public void convert8()
        {
            StringBuilder resnum = new StringBuilder();
            BigInteger num, temp, basis;
            basis = 8;
            num = number;
            do
            {
                num = BigInteger.DivRem(num, basis, out temp);
                resnum.Insert(0, temp);
            } while (!num.Equals(0));
            number = BigInteger.Parse(resnum.ToString());
        }
        public override String ToString()
        {
            return parser.Parse(number.ToString());
        }
    }
}
